public class Main {
    public static void main(String[] args) {
        MyQueue<Integer> mq=new MyQueue();

        mq.add(10);
        mq.add(20);
        mq.add(30);
        mq.add(40);
        mq.add(50);
        System.out.println(mq.isEmpty());

        System.out.println(mq);

        System.out.println(mq.remove(30));
        System.out.println(mq.contains(20));
        System.out.println(mq);

    }
}
